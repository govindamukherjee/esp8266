Update Details:
13. :ESP8266 - Arduino
        1. Add comments
12. :ESP8266 - Arduino
		1. Ticker - for 1 sec interrupt update eventID & temperature outside loop.
		2. send websocket tempC message.
		3. handle websocket delete mem message.
		
	:index.html
		1. tempC - placeholder for Temperature display.
		2. Print button changed to delete mem button.
		3. Remarks textarea max limit set to 74 words.
		4. send websocket delete message.
		5. handle websocket tempC message.
		
	:index_tail.js
		1. handle websocket message for tempC as update
		2. click delete mem button function with conform pop-up ok/cancle.
		
		
	:DS3231-RTC - Arduino
		1. include DS3231-RTC library https://github.com/NorthernWidget/DS3231
		2. Date & Time.
		3. Calculate One Sec from unixtime since midnight 1/1/1970.
		4. Get temperature value.
		
	:Arduino Test 
		1. attach interrupt for pulse counting purpose.
		2. ds3231 rtc test modules.
		3. ESP.h watchdog test. But no use. Hardware watchdog automatic reset the chip if hangs.

11.
10.
9.
8.
7.
6.
5.
4.
3.
2.
1.
------------------------------------------------------------------------------------------------------------------
ESP8266 interface I2C DS3231 RTC: https://techtutorialsx.com/2016/05/22/esp8266-connection-to-ds3231-rtc/
ESP8266 interface I2C OLED: https://randomnerdtutorials.com/esp8266-0-96-inch-oled-display-with-arduino-ide/
ESP8266 interface I2C Counter: https://tinkerman.cat/post/counting-events-with-arduino-and-pcf8583/
ESP8266 read internet webpage: Arduino ESP8266 Basic Http Client Example
ESP8266 deep sleep: https://randomnerdtutorials.com/esp8266-deep-sleep-with-arduino-ide/
ESP8266 millis for time: https://www.best-microcontroller-projects.com/arduino-millis.html
DIP Switch to Voltage Divider: https://www.instructables.com/id/DIP-Tune-Selector-Using-1-Pin/

Input: Bkg, Sampling Time, Bkg Time, Eff, CF
Calculated: LLD, MDA, Bat
DATETIME - format: YYYY-MM-DD HH:MI:SS

struct Config {   
  uint8_t eff;
  unsigned int bkgCPM;
  unsigned int bkgMin;
  unsigned int samMin;
  uint8_t cl;
  unsigned int runMin;
  uint8_t unitSet;  
};

struct LogData{
  unsigned String eventID;
  uint8_t areaID;    //0 to 249 int area=249/10 int lab = 249%area
  unsigned long grossCnts;
  unsigned long periodSec;
  String remarks;
};

unsigned int eventDate;   //yy-mm-dd
unsigned int eventTime;   //hh:mm:ss
  
const char *logFile = "/log20001.json";
const char *configFile = "/config20001.json";  
Config configData;                         // <- global configuration object
LogData logData;

Button: Start, Stop, Reset, Next, Print

opCode: opValue
a: uint8_t areaID; area id
b: unsigned int bkgCPM: bkg in cpm : user input 4 digits
c: unsigned long grossCnts: counts gross
d: delete mem operation
e: uint8_t eff: eff in % : user input 2 digits
f: String eventID
g: unsigned int runMin:
h: uint8_t unitSet:
i:
j:
k: float 
l: uint8_t cl: confidence level : user input 2 digits
m: uint8_t mem: memory filled
n: next button
o: stop button
p: unsigned long periodSec: time period of counting
q:
r: reset button
s: start button
t: uint8_t tempC
u: unsigned int bkgMin: time bkg in min : user input 4 digits
v: unsigned int samMin: time sam in min : user input 4 digits
w:
x: logData sync
y: String remarks: remarks
z: config sync
----------------------------------------------------------------------------------------------------------
Can Be Displayed: Battery, HV Voltage, Temp, 
Things Required

A. 16 channel multiplexer:

4 dip switch for area digital
5 rotary switch for lab digital
5 navigation switch digital
1 hv ref analog 
1 battery analog

B. Buzzer

C. Micro USB Connector

vcc
gnd
hv ref
hv on/off
counts

D. 1.3" SPI OLED

E. 3.3V Zenner

F. NODEMCU 1.0 GPIO 0 - 16
3, 1 : Serial Rx, Tx 
5, 4 : I2C SCL, SDA DS3231, OLED
12, 13, 14, 15, 16: Navigation Switch
10: Navigation switch set button
ESP RESET: Navigation switch reset
9: Rotary encoder switch
0, 2, A0: MULTIPLEXER S0, S1, SIG
00: HV VOLTAGE REF
01: BATT VOLTAGE REF
10: ROTARY SWITCH OUTPUT A
11: ROTARY SWITCH OUTPUT B


G. DS3231 RTC TEMP I2C

H. M274 360 Degree Rotary Encoder Brick Sensor Module

