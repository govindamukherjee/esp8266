/*
  Date: 05-05-2020
*/

// Import required libraries
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <FS.h>
#include <WebSocketsServer.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <ArduinoJson.h>
#include <DS3231.h>
#include <Wire.h>
#include <Ticker.h>

Ticker secTick;  

//Class objects
DS3231 Clock;
RTClib RTC;
DateTime now;

//Date & Time Variables
byte Year;
byte Month;
byte Date;
byte DoW;
byte Hour;
byte Minute;
byte Second;
float tempC = 0.0;
bool Century=false;
bool h12;
bool PM;
unsigned long oldSec = 0;
unsigned long newSec = 0;

// Replace with your network credentials
const char* ssid = "AaronTechSolutions";
const char* password = "GaarontechM@2017";
const char* srNo = "20001";

// Set LED GPIO
const uint8_t espLedPin = 2;
const uint8_t nodeLedPin = 16;
const uint8_t num = 0;
uint8_t mem = 0;
unsigned int timeInMin = 0;

struct Config {
  uint8_t eff;
  unsigned int bkgCPM;
  unsigned int bkgMin;
  unsigned int samMin;
  uint8_t cl;
  unsigned int runMin;
  uint8_t unitSet;
};

struct LogData{
  String eventID;
  uint8_t areaID;    //0 to 255 int area=255/10 int lab = 255%area
  unsigned long grossCnts;
  unsigned long periodSec;
  String remarks;
};
 
String messageSend = "";

const char *logFile = "/log20001.json";
const char *configFile = "/config20001.json"; 
Config configData;                         
LogData logData;

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);
WebSocketsServer webSocket = WebSocketsServer(81);

void setDateTime(String eventID) //extract date and time from eventID
{
  Serial.println(eventID);
  Year = eventID.substring(0, 2).toInt();
  Month = eventID.substring(2, 4).toInt();
  Date = eventID.substring(4, 6).toInt();
  Hour = eventID.substring(6, 8).toInt();
  Minute = eventID.substring(8, 10).toInt();
  Second = eventID.substring(10, 12).toInt();

  Clock.setYear(Year);
  Clock.setMonth(Month);
  Clock.setDate(Date);
  Clock.setHour(Hour);
  Clock.setMinute(Minute);
  Clock.setSecond(Second);
}

void updateDateTime() //Arduino to Webpage: Temperature & eventID
{
  Year = Clock.getYear();
  Month = Clock.getMonth(Century);
  Date = Clock.getDate();
  DoW = Clock.getDoW();
  Hour = Clock.getHour(h12, PM);
  Minute = Clock.getMinute();
  Second = Clock.getSecond();

  tempC = Clock.getTemperature();

  messageSend = "20001#t#u#"+ String(tempC, 2);
  //Serial.println(messageSend);
  webSocket.broadcastTXT(messageSend);

  //Serial.println(String(tempC, 3));

  logData.eventID = String(Year, DEC);
  if(Month < 10)
  {
    logData.eventID = logData.eventID + "0" + String(Month, DEC);
  }
  else
  {
    logData.eventID = logData.eventID + String(Month, DEC);
  }

  if(Date < 10)
  {
    logData.eventID = logData.eventID + "0" + String(Date, DEC);
  }
  else
  {
    logData.eventID = logData.eventID + String(Date, DEC);
  }

  if(Hour < 10)
  {
    logData.eventID = logData.eventID + "0" + String(Hour, DEC);
  }
  else
  {
    logData.eventID = logData.eventID + String(Hour, DEC);
  }

  if(Minute < 10)
  {
    logData.eventID = logData.eventID + "0" + String(Minute, DEC);
  }
  else
  {
    logData.eventID = logData.eventID + String(Minute, DEC);
  }

  if(Second < 10)
  {
    logData.eventID = logData.eventID + "0" + String(Second, DEC);
  }
  else
  {
    logData.eventID = logData.eventID + String(Second, DEC);
  }

  //Serial.println(logData.eventID);

  messageSend = "20001#f#u#"+ logData.eventID;
  //Serial.println(messageSend);
  webSocket.broadcastTXT(messageSend);
}

void stopOperation() //Stop Counting, Save Data, Update Webpage
{
  timeInMin = 0;
  messageSend = "20001#a#u#"+String(logData.areaID, 10);
  webSocket.broadcastTXT(messageSend);
  delay(200);
  messageSend = "20001#b#u#"+ String(configData.bkgCPM, 10);
  webSocket.broadcastTXT(messageSend);
  delay(200);
  messageSend = "20001#c#u#"+String(logData.grossCnts, 10);
  webSocket.broadcastTXT(messageSend);
  delay(200);
  messageSend = "20001#e#u#"+String(configData.eff, 10);
  webSocket.broadcastTXT(messageSend);
  delay(200);
  messageSend = "20001#f#u#"+ logData.eventID;
  webSocket.broadcastTXT(messageSend);
  delay(200);
  messageSend = "20001#p#u#"+String(logData.periodSec, 10);
  webSocket.broadcastTXT(messageSend);
  delay(200);
  if(appendData(logFile ,logDataJsonString()))
  {
    webSocket.broadcastTXT("20001#o#o#");
  }
  logData.periodSec = 0;
}

void splitPayload(String message) //Analyze WebPage to Arduino messages for necessary operations
{
  if((String)message.substring(0, 5) == srNo)
  {
    if((String)message.substring(6,7) == "a")
    {
      if((String)message.substring(8,9) == "o")
      {
        //Serial.println("location button");
        messageSend = (String)message.substring(10);
        logData.areaID = messageSend.toInt();
        //Serial.println(logData.areaID);
        messageSend = "20001#a#o#"+messageSend;
        webSocket.broadcastTXT(messageSend);
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("location button ack");
      }
    }
    else if((String)message.substring(6,7) == "b")
    {
      if((String)message.substring(8,9) == "o")
      {
        //Serial.println("bkgCPM button");
        messageSend = (String)message.substring(10);
        configData.bkgCPM = messageSend.toInt();
        //Serial.println(configData.bkgCPM);
        if(saveConfiguration())
        {
          messageSend = "20001#b#o#"+messageSend;
          webSocket.broadcastTXT(messageSend);
        }
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("bkgCPM button ack");
      }
    }
    else if((String)message.substring(6,7) == "d")
    {
      if((String)message.substring(8,9) == "o")
      {
        Serial.println("delete button");
        deleteData(logFile);
        delay(200);
        updateMemorySize();
        messageSend = "20001#m#u#"+ String(mem, 10);
        Serial.println(messageSend);
        webSocket.broadcastTXT(messageSend);
      }
      else if((String)message.substring(8,9) == "a")
      {
        Serial.println("delete button ack");
      }
    }
    else if((String)message.substring(6,7) == "e")
    {
      if((String)message.substring(8,9) == "o")
      {
        //Serial.println("eff button");
        messageSend = (String)message.substring(10);
        configData.eff = messageSend.toInt();
        //Serial.println(configData.eff);
        if(saveConfiguration())
        {
          messageSend = "20001#e#o#"+messageSend;
          webSocket.broadcastTXT(messageSend);
        }
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("eff button ack");
      }
    }
    else if((String)message.substring(6,7) == "f")
    {
      if((String)message.substring(8,9) == "u")
      {
        Serial.println("eventID");
        logData.eventID = (String)message.substring(10);
        setDateTime(logData.eventID);
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("eventID ack");
      }
    }
    else if((String)message.substring(6,7) == "g")
    {
      if((String)message.substring(8,9) == "o")
      {
        //Serial.println("Run Min Radio Button");
        messageSend = (String)message.substring(10);
        configData.runMin = messageSend.toInt();
        //Serial.println(configData.runMin);
        if(saveConfiguration())
        {
          messageSend = "20001#g#o#"+messageSend;
          webSocket.broadcastTXT(messageSend);
        }
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("Run Min Radio Button ack");
      }
    }
    else if((String)message.substring(6,7) == "h")
    {
      if((String)message.substring(8,9) == "o")
      {
        //Serial.println("Unit Radio Button");
        messageSend = (String)message.substring(10);
        configData.unitSet = messageSend.toInt();
        //Serial.println(configData.unitSet);
        if(saveConfiguration())
        {
          messageSend = "20001#h#o#"+messageSend;
          webSocket.broadcastTXT(messageSend);
        }
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("Unit Radio Button ack");
      }
    }
    else if((String)message.substring(6,7) == "l")
    {
      if((String)message.substring(8,9) == "o")
      {
        //Serial.println("cl button");
        messageSend = (String)message.substring(10);
        configData.cl = messageSend.toInt();
        //Serial.println(configData.cl);
        if(saveConfiguration())
        {
          messageSend = "20001#l#o#"+messageSend;
          webSocket.broadcastTXT(messageSend);
        }
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("cl button ack");
      }
    }
    else if((String)message.substring(6,7) == "n")
    {
      if((String)message.substring(8,9) == "o")
      {
        //Serial.println("next button");
        //webSocket.broadcastTXT("20001#y#g#");
        //delay(200);
        stopOperation();
        logData.areaID = logData.areaID + 1;
        if((logData.areaID > 4) && (logData.areaID < 10))
        {
          logData.areaID = 10;
        }
        else if((logData.areaID > 14) && (logData.areaID < 20))
        {
          logData.areaID = 20;
        }
        else if((logData.areaID > 24) && (logData.areaID < 30))
        {
          logData.areaID = 30;
        }
        else if((logData.areaID > 34) && (logData.areaID < 40))
        {
          logData.areaID = 40;
        }
        else if(logData.areaID > 44)
        {
          logData.areaID = 44;
        }

        messageSend = "20001#a#u#"+String(logData.areaID, 10);
        webSocket.broadcastTXT(messageSend);
        now = RTC.now();
        oldSec = now.unixtime();
        timeInMin = configData.runMin;
        //webSocket.broadcastTXT("20001#n#o#");
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("next button ack");
      }
    }
    else if((String)message.substring(6,7) == "o")
    {
      if((String)message.substring(8,9) == "o")
      {
        //Serial.println("stop button");
        stopOperation();
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("location button ack");
      }
    }
    else if((String)message.substring(6,7) == "r")
    {
      if((String)message.substring(8,9) == "o")
      {
        //Serial.println("reset button");
        logData.periodSec = 0;
        logData.grossCnts = 0;
        //webSocket.broadcastTXT("20001#r#u");
        messageSend = "20001#c#u#"+ String(logData.grossCnts, 10);
        webSocket.broadcastTXT(messageSend);
        messageSend = "20001#p#u#"+ String(logData.periodSec, 10);
        webSocket.broadcastTXT(messageSend);
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("reset button ack");
      }
    }
    else if((String)message.substring(6,7) == "s")
    {
      if((String)message.substring(8,9) == "o")
      {
        //Serial.println("start button");
        webSocket.broadcastTXT("20001#y#g#");
        delay(200);
        webSocket.broadcastTXT("20001#s#o#");

        now = RTC.now();
        oldSec = now.unixtime();
        timeInMin = configData.runMin;
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("start button ack");
      }
    }
    else if((String)message.substring(6,7) == "u")
    {
      if((String)message.substring(8,9) == "o")
      {
        //Serial.println("bkgMin button");
        messageSend = (String)message.substring(10);
        configData.bkgMin = messageSend.toInt();
        //Serial.println(configData.bkgMin);
        if(saveConfiguration())
        {
          messageSend = "20001#u#o#"+messageSend;
          webSocket.broadcastTXT(messageSend);
        }
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("bkgMin button ack");
      }
    }
    else if((String)message.substring(6,7) == "v")
    {
      if((String)message.substring(8,9) == "o")
      {
        //Serial.println("samMin button");
        messageSend = (String)message.substring(10);
        configData.samMin = messageSend.toInt();
        //Serial.println(configData.samMin);
        if(saveConfiguration())
        {
          messageSend = "20001#v#o#"+messageSend;
          webSocket.broadcastTXT(messageSend);
        }
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("samMin button ack");
      }
    }
    else if((String)message.substring(6,7) == "y")
    {
      if((String)message.substring(8,9) == "o")
      {
        //Serial.println("remarks operation");
        logData.remarks = (String)message.substring(10);
        Serial.println(logData.remarks);
        //messageSend = "20001#y#o#";
        //webSocket.broadcastTXT(messageSend);
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("remarks ack");
      }
    }
    else if((String)message.substring(6,7) == "z")  //WebPage onload updates
    {
      if((String)message.substring(8,9) == "o")
      {
        sendLogData();
        delay(200);
        messageSend = "20001#e#u#"+ String(configData.eff, 10);
        //Serial.println(messageSend);
        webSocket.broadcastTXT(messageSend);
        delay(200);
        messageSend = "20001#u#u#"+ String(configData.bkgMin, 10);
        //Serial.println(messageSend);
        webSocket.broadcastTXT(messageSend);
        delay(200);
        messageSend = "20001#v#u#"+ String(configData.samMin, 10);
        //Serial.println(messageSend);
        webSocket.broadcastTXT(messageSend);
        delay(200);
        messageSend = "20001#l#u#"+ String(configData.cl, 10);
        //Serial.println(messageSend);
        webSocket.broadcastTXT(messageSend);
        delay(200);
        messageSend = "20001#g#u#"+ String(configData.runMin, 10);
        //Serial.println(messageSend);
        webSocket.broadcastTXT(messageSend);
        delay(200);
        messageSend = "20001#b#u#"+ String(configData.bkgCPM, 10);
        //Serial.println(messageSend);
        webSocket.broadcastTXT(messageSend);
        delay(200);
        messageSend = "20001#h#u#"+ String(configData.unitSet, 10);
        //Serial.println(messageSend);
        webSocket.broadcastTXT(messageSend);
        delay(200);
        messageSend = "20001#f#u#"+ logData.eventID;
        //Serial.println(messageSend);
        webSocket.broadcastTXT(messageSend);
        delay(200);
        messageSend = "20001#m#u#"+ String(mem, 10);
        //Serial.println(messageSend);
        webSocket.broadcastTXT(messageSend);
        delay(200);
        messageSend = "20001#t#u#"+ String(tempC, 2);
        Serial.println(messageSend);
        webSocket.broadcastTXT(messageSend);
      }
      else if((String)message.substring(8,9) == "a")
      {
        //Serial.println("Config Sync ack");
      }
    }
  }
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {
    switch(type)
    {
        case WStype_DISCONNECTED:
        {
            Serial.print("Disconnected! ");
            Serial.println(num);
        }
        break;

        case WStype_CONNECTED:
        {
            IPAddress ip = webSocket.remoteIP(num);
            Serial.print("Connected from ");
            Serial.println(num);
            for(uint8_t i = 0; i<4; i++)
            {
              Serial.print(ip[i]);
            }
            Serial.print("\n url: ");
            webSocket.sendTXT(num, "Connected");
        }
        break;

        case WStype_TEXT:
        {
          String message = "";
          for(int i = 0; i < length; i++)
          {
            message = message + (char)payload[i];
          }
          Serial.print(message);
          Serial.println();

          splitPayload(message);

        }
        break;
    }
}

void myClient()     //for pushing saved data from logFile to RaspberryPi Database.
{ 

  WiFiClient client;

  HTTPClient http;

  //Serial.print("[HTTP] begin...\n");
  if (http.begin(client, "http://help.websiteos.com/websiteos/example_of_a_simple_html_page.htm")) {  // HTTP


    //Serial.print("[HTTP] GET...\n");
    // start connection and send HTTP header
    int httpCode = http.GET();

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      //Serial.printf("[HTTP] GET... code: %d\n", httpCode);

      // file found at server
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
        String payload = http.getString();
        //Serial.println(payload);
      }
    } else {
      //Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
  } else {
    //Serial.printf("[HTTP} Unable to connect\n");
  }
}

void deleteData(const char * fileToDelete)  //Delete all the data in the file and bring the cursor to starting point.
{
  File file = SPIFFS.open(fileToDelete, "w");

  if (!file) {
    Serial.println("Error opening file for writing");
    return;
  }

  int bytesWritten = file.print(" ");

  if (bytesWritten > 0) {
    Serial.println("Delete File was written");
    Serial.println(bytesWritten);

  } else {
    Serial.println("File write failed");
  }

  file.close();
}

void updateMemorySize()  //SPIFFS Memory Size
{
  //Serial.print("Flash Real Size: ");
  //Serial.println(ESP.getFlashChipRealSize());
  //Serial.print("Flash Firmware Configured Size: ");
  //Serial.println(ESP.getFlashChipSize());

  FSInfo fsInfo;
  SPIFFS.info(fsInfo);
  //Serial.print("FS Bytes: ");
  unsigned int int_usedBytes = fsInfo.usedBytes;
  unsigned int int_totalBytes = fsInfo.totalBytes;
  //Serial.print(int_usedBytes);
  //Serial.print(" / ");
  //Serial.println(int_totalBytes);

  float perBytes = ((int_usedBytes*100.0)/int_totalBytes);
  String str_mem = String(perBytes, 1);
  //Serial.println(str_mem);
  mem = perBytes;
  //Serial.println(mem);
  if(perBytes > 95.0)
  {
    deleteData(logFile);
  }

}

void loadConfiguration() //Read configFile.json and set the necessary variables default values.
{
  Serial.println("Loading CONFIG file");
  File file = SPIFFS.open(configFile, "r");
  if (!file) {
    Serial.println("Error opening file for reading");
    Serial.println(F("Creating config20001.json and Saving default configuration..."));
    saveConfiguration();
    delay(200);
    file = SPIFFS.open(configFile, "r");
    if (!file) {
        Serial.println("Error opening file for reading");
        return;
    }
  }

  StaticJsonDocument<512> doc;

  DeserializationError error = deserializeJson(doc, file);
  if (error)
    Serial.println(F("Failed to read file, using default configuration"));

  configData.eff = doc["eff"] | 25;
  configData.bkgCPM = doc["bkgCPM"] | 1;
  configData.bkgMin = doc["bkgMin"] | 1;
  configData.samMin = doc["samMin"] | 1;
  configData.cl = doc["cl"] | 95;
  configData.runMin = doc["runMin"] | 1;
  configData.unitSet = doc["unitSet"] | 0;

  file.close();
}

bool saveConfiguration() //Update the configFile from user input in webpage
{
  Serial.println("Saving CONFIG File");
  File file = SPIFFS.open(configFile, "w+");
  if (!file) {
    Serial.println("Error opening file for writing");
    return false;
  }

  StaticJsonDocument<256> doc;

  doc["eff"] = configData.eff;
  doc["bkgCPM"] = configData.bkgCPM;
  doc["bkgMin"] = configData.bkgMin;
  doc["samMin"] = configData.samMin;
  doc["cl"] = configData.cl;
  doc["runMin"] = configData.runMin;
  doc["unitSet"] = configData.unitSet;

  if (serializeJson(doc, file) == 0) {
    Serial.println(F("Failed to write to file"));
  }

  // Close the file
  file.close();
  return true;
}

bool appendData(const char * fileToSave, String dataToAppend)  //Append the logFile
{
  Serial.println("Appending LogFile");
  File file = SPIFFS.open(fileToSave, "a+");

  if (!file) {
    Serial.println("Error opening file for writing");
    return false;
  }

  int bytesWritten = file.print(dataToAppend);

  if (bytesWritten > 0) {
    Serial.println("File was written");
    Serial.println(bytesWritten);

  } else {
    Serial.println("File write failed");
  }

  file.close();

  return true;
}

String logDataJsonString()  //Convert variables to jason format for saving in logFile
{
  String dataToAppend = "\n$";
  StaticJsonDocument<200> doc;

  doc["eventID"] = logData.eventID;

  doc["eff"] = configData.eff;
  doc["bkgCPM"] = configData.bkgCPM;

  doc["areaID"] = logData.areaID;
  doc["grossCnts"] = logData.grossCnts;
  doc["periodSec"] = logData.periodSec;
  doc["remarks"] = logData.remarks;

  serializeJson(doc, dataToAppend);
  dataToAppend = dataToAppend + "#";
  Serial.println(dataToAppend);
  return dataToAppend;
}

void sendLogData()  //Update the webpage page with logFile data
{
  String fileData = "";
  Serial.println("Send Log file");
  File file = SPIFFS.open(logFile, "r");
  if (file)
  {
    char currentChar = 'a';
    bool startRecording = false;
    while(file.available())
    {
      currentChar = (char)file.read();
      if(currentChar == '#')
      {
        startRecording = false;
        fileData = "20001#x#u#" + fileData;
        webSocket.broadcastTXT(fileData);
        Serial.println(fileData);
        fileData = "";
        continue;
      }
      else if(currentChar == '$')
      {
        startRecording = true;
        continue;
      }

      if(startRecording)
      {
        fileData = fileData + currentChar;
      }
    }
    file.close();
  }
  else
  {
    Serial.println("Error opening file for reading");
    return;
  }
}

void setup()
{
  configData.eff = 25;
  configData.bkgCPM = 1;
  configData.bkgMin = 1;
  configData.samMin = 1;
  configData.cl = 95;
  configData.runMin = 1;
  configData.unitSet = 0;

  logData.grossCnts = 0;
  logData.periodSec = 0;
  logData.areaID = 0;
  logData.eventID = "200605110933";
  logData.remarks = "remarks";

  Wire.begin();
  delay(10);

  Clock.setClockMode(false);  // set to 24h
  Clock.enable32kHz(true);
  Clock.enableOscillator(true, false, 0);



  //Serial port for debugging purposes
  Serial.begin(115200);
  pinMode(espLedPin, OUTPUT);
  pinMode(nodeLedPin, OUTPUT);

  digitalWrite(espLedPin, LOW);
  digitalWrite(nodeLedPin, LOW);

  // Initialize SPIFFS
  if(!SPIFFS.begin()){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }
  else
  {
    loadConfiguration();   //files should be created before publishing
    appendData(logFile, " ");
    //deleteData(logFile);
    updateMemorySize();
  }

  // Connect to Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  // Print ESP32 Local IP Address
  Serial.println(WiFi.localIP());

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html");
  });

  // Route for root configFile web page
  server.on(configFile, HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, configFile, "text/json");
  });

  // Route for root logFile web page
  server.on(logFile, HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, logFile, "text/json");
  });

  // Route to load bootstrap.css file
  server.on("/css/bootstrap.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/css/bootstrap.css", "text/css");
  });

  // Route to load index.css file
  server.on("/css/index.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/css/index.css", "text/css");
  });

  // Route to load bootstrap.js file
  server.on("/js/bootstrap.js", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/js/bootstrap.js", "text/js");
  });

  // Route to load jquery.js file
  server.on("/js/jquery.js", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/js/jquery.js", "text/js");
  });

  // Route to load index_head.js file
  server.on("/js/index_head.js", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/js/index_head.js", "text/js");
  });

  // Route to load index_tail.js file
  server.on("/js/index_tail.js", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/js/index_tail.js", "text/js");
  });

  // Start server
  server.begin();
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);

  secTick.attach(1, updateDateTime);   //Operations to perform in every 1 sec
}

void loop()
{
  webSocket.loop();

  if(timeInMin > 0)
  {
    now = RTC.now();
    newSec = now.unixtime();     //Counting time is derived from DS3231 unixtime. So that any lagging in ESP8266 will not effect the counting time.
    if((newSec - oldSec) > 0)
    {
      logData.periodSec = logData.periodSec + (newSec - oldSec);
      messageSend = "20001#c#u#"+ String(logData.grossCnts, 10);
      webSocket.broadcastTXT(messageSend);
      messageSend = "20001#p#u#"+ String(logData.periodSec, 10);
      webSocket.broadcastTXT(messageSend);
      oldSec = newSec;
      if((logData.periodSec/60) >= timeInMin)
      {
        //Serial.println("time up");
        stopOperation();
      }
    }
  }
}
